using Assets.Scripts;
using Assets.Scripts.Persistance;
using UnityEngine;
using Zenject;

public class ZenInstaller : MonoInstaller<ZenInstaller>
{
    public override void InstallBindings()
    {         
        Container.Bind<IGameLogger>().To<GameLogger>().AsSingle();    
    }
}