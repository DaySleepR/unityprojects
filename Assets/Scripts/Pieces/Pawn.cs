﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Pieces
{
    public class Pawn : Piece
    {
        /// <summary>
        /// Pawn movement rules.
        /// </summary>
        /// <param name="src"></param>
        /// <param name="dst">Board position piece is moving to</param>
        /// <returns></returns>
        public override bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = false;
            BoardPosition src = this.BoardPosition;

            // direction pawn can move based on color
            int direction = Color == PieceColor.Black ? -1 : 1;

            // pawn movement without taking enemy piece
            // ****************************************            

            // must stay in same column
            if (src.column == dst.column)
            {
                // can move one tile 
                if (dst.row == src.row + (1 * direction))
                {
                    // if there is no piece at tile
                    Piece dstPiece = base.Board.PieceAtPosition(dst);

                    if (dstPiece == null)
                    {
                        ret = true;
                    }
                }

                // pawn hasn't moved yet and moves two tiles forward
                else if ((!this.HasMoved) && (dst.row == src.row + (2 * direction)))
                {
                    Piece dstPiece = base.Board.PieceAtPosition(dst);

                    BoardPosition beforeDst = new BoardPosition(dst.column, dst.row - (1 * direction));
                    Piece beforeDstPiece = base.Board.PieceAtPosition(beforeDst);

                    // there must be no pieces at either tiles
                    if (dstPiece == null && beforeDstPiece == null)
                    {
                        return true;
                    }
                }

            }
            // when taking pieces pawn can move one tile forward and one tile to either side
            else if ( (dst.column == (src.column + 1) || dst.column == (src.column - 1)) && dst.row == src.row + (1 * direction) )
            {
                // if there is a piece to take at destination
                Piece dstPiece = base.Board.PieceAtPosition(dst);

                // pawn can take a piece of opposite color except if it's a king
                if (dstPiece != null && dstPiece.Color != this.Color && dstPiece.Type != PieceType.King)
                {
                    return true;
                }
            }

            return ret;
        }

    }
}
