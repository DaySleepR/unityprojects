﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Pieces
{
    public class Knight : Piece
    {
        public override bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = false;

            BoardPosition src = this.BoardPosition;

            if (dst == src) return false;

            int rOffset = Math.Abs(dst.row - src.row);
            int cOffset = Math.Abs(dst.column - src.column);

            if ((rOffset == 1 && cOffset == 2) || (rOffset == 2 && cOffset == 1))
            {
                if (CanTakeAtPosition(dst, kingCheckMove))
                {
                    ret = true;
                }             
            }

            return ret;

        }
    }
}
