﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public delegate void TileClickedDelegate(Tile t);


public class Tile : MonoBehaviour
{
    public BoardPosition BoardPosition;

    public bool TileHovered;
    public bool Selected;


    GameObject highlightObj;
    Renderer highlightRenderer;

    GameObject selectedObj;
    Renderer selectedRenderer;

    public event TileClickedDelegate TileClicked;
    public event TileClickedDelegate TileRightClicked;


    private void FireTileClicked()
    {
        if (TileClicked != null)
            TileClicked(this);
    }

    private void FireTileRightClicked()
    {
        if (TileRightClicked != null)
            TileRightClicked(this);
    }


    void Start()
    {
        TileHovered = false;

        highlightObj = this.transform.Find("TileHighlight").gameObject;
        highlightRenderer = highlightObj.GetComponent<Renderer>();

        selectedObj = this.transform.Find("TileSelected").gameObject;
        selectedRenderer = selectedObj.GetComponent<Renderer>();
    }
    
    void Update()
    {
        CheckTileHovered();
        RenderHighlight();
    }

    private void CheckTileHovered()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;     

        if (Physics.Raycast(ray, out hit))
        {
            Tile tile = hit.transform.gameObject.GetComponent<Tile>();

            if (tile != null && (tile.BoardPosition == this.BoardPosition))
            {
                TileHovered = true;

                if (Input.GetMouseButtonDown(0))
                {                   
                    FireTileClicked();
                }
            }
            else
            {
                TileHovered = false;
            }
        }
        else
        {
            TileHovered = false;
        }
    }


    private void RenderHighlight()
    {
        selectedRenderer.enabled = Selected;
        highlightRenderer.enabled = TileHovered && (!Selected);        
    }  


}
