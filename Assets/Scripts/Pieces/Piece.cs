﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Pieces
{
    public class Piece : MonoBehaviour
    {
        public PieceColor Color;
        public PieceType Type;

        public bool HasMoved { get; set; }

        private BoardPosition _initialPosition;

        public BoardPosition BoardPosition;
        public BoardPosition InitialPosition
        {
            get
            {
                return _initialPosition;
            }
            set
            {
                _initialPosition = value;
                BoardPosition = value;
                HasMoved = false;
            }
        }

        private BoardPosition _futurePosition;

        public BoardPosition FuturePosition
        {
            get { return _futurePosition; }
            set { _futurePosition = value; }
        }

        public BoardPosition CheckRelevantPosition
        {
            get
            {
                if (FuturePosition != null) return FuturePosition;
                else return BoardPosition;
            }
        }


        bool _selected = false;

        public Tile CurrentTile;

        protected IBoard Board { get; set; }

        public bool Selected
        {
            get
            {
                return _selected;
            }
            set
            {
                _selected = value;
                AnimateSelectedState();
            }
        }

        void Start()
        {
            Board = GameObject.FindWithTag("Board").GetComponent<Board>();
        }

        void Update()
        {

        }

        public void MoveToTile(Tile t)
        {
            if (t != null)
            {
                // move to tile
                this.transform.SetPositionAndRotation(t.transform.position, this.transform.rotation);

                if (this.BoardPosition != t.BoardPosition)
                {
                    this.HasMoved = true;
                }

                // set chess coordinates
                this.BoardPosition = t.BoardPosition;
                this.Selected = false;

                CurrentTile = t;

                FuturePosition = null;

            }
        }

        void AnimateSelectedState()
        {
            if (_selected)
            {
                gameObject.transform.position = gameObject.transform.position + new Vector3(0f, 0.3f, 0);
            }
            else
            {
                gameObject.transform.position = gameObject.transform.position + new Vector3(0f, 0f, 0f);
            }
        }

        public virtual bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = true;

            return ret;
        }

        protected bool CanTakeAtPosition(BoardPosition dst, bool kingCheckMove = false)
        {
            Piece p = Board.PieceAtPosition(dst);
            if (p == null)
            {
                return true;
            }
            else
            {
                if (kingCheckMove)
                {
                    return (p.Color != this.Color);
                }
                else
                {
                    return (p.Color != this.Color && p.Type != PieceType.King);
                }
            }
        }

       

        protected bool CheckDiagonalRangeEmpty(BoardPosition start, BoardPosition end, bool kingCheckMode = false)
        {
            bool empty = true;

            int rowDirection = end.row > start.row ? 1 : -1;
            int columnDirection = end.column > start.column ? 1 : -1;

            int diff = Mathf.Abs(start.row - end.row);

            for (int i = 1; i < diff; i++)
            {
                bool pieceExists = TryForPiece(start.column + (i * columnDirection), start.row + (i * rowDirection), kingCheckMode);

                if (pieceExists)
                {
                    empty = false;
                    break;
                }
            }

            return empty;
        }

        protected bool CheckHorizontalVerticalRangeEmpty(BoardPosition start, BoardPosition end, bool kingCheckMode = false)
        {
            bool empty = true;

            if (start.column == end.column)
            {
                // moving vertically
                if (start.row > end.row)
                {
                    for (int r = start.row - 1; r > end.row; r--)
                    {
                        bool pieceExists = TryForPiece(start.column, r, kingCheckMode);

                        if (pieceExists)
                        {
                            empty = false;
                            break;
                        }
                    }
                }
                else
                {
                    for (int r = start.row + 1; r < end.row; r++)
                    {
                        bool pieceExists = TryForPiece(start.column, r, kingCheckMode);

                        if (pieceExists)
                        {
                            empty = false;
                            break;
                        }

                    }
                }
            }
            else
            {
                // moving horizontally
                if (start.column > end.column)
                {
                    for (int c = start.column - 1; c > end.column; c--)
                    {                       

                        bool pieceExists = TryForPiece(c, start.row, kingCheckMode);

                        if (pieceExists)
                        {
                            empty = false;
                            break;
                        }
                    }                   
                }
                else
                {
                    for (int c = start.column + 1; c < end.column; c++)
                    {                      
                        bool pieceExists = TryForPiece(c, start.row, kingCheckMode);

                        if (pieceExists)
                        {
                            empty = false;
                            break;
                        }
                    }
                }
            }

            return empty;
        }

        private bool TryForPiece(int c, int r, bool kingCheckMode)
        {
            bool ok = false;

            Piece p = null;

            if (kingCheckMode)
            {
                p = Board.PieceAtCheckRelevantPosition(new BoardPosition(c, r));
            }
            else
            {
                p = Board.PieceAtPosition(new BoardPosition(c, r));
            }

            ok = p != null;

            return ok;

        }


    }

}