﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Persistance
{
    public interface IGameLogger
    {
        void LogMove(BoardPosition src, BoardPosition dst);
    }

    public class GameLogger : IGameLogger
    {   
        Game game;

        private string SavePath
        {
            get
            {
                return Path.Combine(Application.persistentDataPath, "ChessFile.txt");
            }
        }

        public GameLogger()
        {
            game = new Game();
        }

        public void LogMove(BoardPosition src, BoardPosition dst)
        {          
            game.Add(new Move(src, dst));
        }

        public void Save()
        {
           string jsonData = JsonUtility.ToJson(game);

            using (StreamWriter streamWriter = File.CreateText(SavePath))
            {
                streamWriter.Write(jsonData);
            }
        }

        public void Load(string path)
        {
            try
            {
                using (StreamReader streamReader = File.OpenText(SavePath))
                {
                    string jsonData = streamReader.ReadToEnd();
                    game = JsonUtility.FromJson<Game>(jsonData);
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Error loading game", ex);
            }
            
        }

    }
}
