﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class BoardPosition
{
    public int column;
    public int row;

    public BoardPosition(int c, int r)
    {
        this.column = c;
        this.row = r;           
    }

    public BoardPosition Clone()
    {
        BoardPosition bp = new BoardPosition(this.column, this.row);
        return bp;
    }

    public Vector3 Vector
    {
        get
        {
            // translate chess coordinates to board object coordinates

            int x = column - 1;
            int z = row - 1;

            Vector3 v = new Vector3(x, 0, z);
            return v;
        }
    }

   

    public override string ToString()
    {
        return String.Format("{0}{1}", ((char)(column + 96)), row.ToString());
    }

    #region Operators

    public static bool operator ==(BoardPosition a, BoardPosition b)
    {
        bool ret = false;

        if (object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) return true;
        if (object.ReferenceEquals(a, null) && !object.ReferenceEquals(b, null)) return false;
        if (!object.ReferenceEquals(a, null) && object.ReferenceEquals(b, null)) return false;               

        ret = a.Equals(b);

        return ret;
    }

    public static bool operator !=(BoardPosition a, BoardPosition b)
    {
        bool ret = false;

        ret = !(a == b);

        return ret;
    }

    public override bool Equals(object other)
    {
        bool eq = false;

        if (other != null && other.GetType() == typeof(BoardPosition))
        {
            BoardPosition ot = (BoardPosition)other;

            if (this.row == ot.row && this.column == ot.column)
            {
                eq = true;
            }
        }

        return eq;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    #endregion

}

