﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


    public class Player
    {
        public Player(string name, PieceColor color)
        {
            Name = name;
            Color = color;
            Id = Guid.NewGuid();
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public PieceColor Color { get; set; }

    }

