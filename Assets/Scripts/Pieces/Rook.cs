﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Pieces
{
    public class Rook : Piece
    {
        public override bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = false;

            BoardPosition src = this.BoardPosition;
            if (dst == src) return false;

            // rook can move either vertically or horizontally
            if (src.column == dst.column || src.row == dst.row)
            {
                bool pathClear = CheckHorizontalVerticalRangeEmpty(src, dst, kingCheckMove);

                if (pathClear)
                {
                    if (CanTakeAtPosition(dst, kingCheckMove))
                    {
                        ret = true;
                    }
                }
            }

            return ret;
        }

    }
}
