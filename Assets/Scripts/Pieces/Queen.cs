﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Pieces
{
    public class Queen : Piece
    {
        public override bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = false;

            BoardPosition src = this.BoardPosition;
            if (dst == src) return false;

            bool moveVerticalHorizontal = src.column == dst.column || src.row == dst.row;
            bool moveDiagonal = Math.Abs(dst.column - src.column) == Math.Abs(dst.row - src.row);

            if ((moveVerticalHorizontal) || (moveDiagonal))
            {
                bool pathClear = false;

                if (moveVerticalHorizontal)
                {
                    pathClear = CheckHorizontalVerticalRangeEmpty(src, dst,kingCheckMove);
                }
                else
                {
                    pathClear = CheckDiagonalRangeEmpty(src, dst, kingCheckMove);
                }

                if (pathClear)
                {
                    if (CanTakeAtPosition(dst, kingCheckMove))
                    {
                        ret = true;
                    }
                }          

            }

            return ret;
        }

    }
}
