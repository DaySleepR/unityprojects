﻿using Assets.Scripts.Pieces;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Zenject;

public delegate void MovePlayedHandler(Piece piece, BoardPosition origin, BoardPosition destination);

public interface IBoard
{
    event MovePlayedHandler MovePlayed;

    void SwitchPlaces();
    Piece PieceAtPosition(BoardPosition position);
    Piece PieceAtCheckRelevantPosition(BoardPosition position);
}

public class Board : MonoBehaviour, IBoard
{
    public GameObject TilePrefab; 

    public GameObject WhitePawnPrefab;
    public GameObject WhiteRookPrefab;
    public GameObject WhiteKnightPrefab;
    public GameObject WhiteBishopPrefab;
    public GameObject WhiteQueenPrefab;
    public GameObject WhiteKingPrefab;

    public GameObject BlackPawnPrefab;
    public GameObject BlackRookPrefab;
    public GameObject BlackKnightPrefab;
    public GameObject BlackBishopPrefab;
    public GameObject BlackQueenPrefab;
    public GameObject BlackKingPrefab;

    public BoardState State { get; set; }
    public IGameManager GameManager { get; private set; }

    PieceColor boardSide;
    GameObject BoardHolder;  

    float whiteRotAngle = 0;
    float blackRotAngle = 180;

    float currentRotAngle = 0;

    float smoothTime = 0.05f;

    public event MovePlayedHandler MovePlayed;

    public void FireMovePlayed(Piece piece, BoardPosition origin, BoardPosition destination)
    {
        if (MovePlayed != null)
            MovePlayed(piece, origin, destination);
    }

    public Piece PieceAtPosition(BoardPosition position)
    {
        return ActivePieces.Where(x => x.BoardPosition == position).FirstOrDefault<Piece>();
    }

    public Piece PieceAtCheckRelevantPosition(BoardPosition position)
    {
        return ActivePieces.Where(x => x.CheckRelevantPosition == position).FirstOrDefault<Piece>();
    }

    public Piece [] ActivePieces
    {
        get
        {
            return this.GetComponentsInChildren<Piece>();
        }
    }

    public Tile [] Tiles
    {
        get
        {
            return this.GetComponentsInChildren<Tile>();
        }
    }

    public Piece SelectedPiece
    {
        get
        {
            return ActivePieces.AsQueryable().Where(x => x.Selected == true).FirstOrDefault<Piece>();
        }
    }

    public Tile SelectedTile
    {
        get
        {
            return Tiles.AsQueryable().Where(x => x.Selected == true).FirstOrDefault<Tile>();
        }
    }

    #region EventHandlers

    public void TileClickedEventHandler(Tile t)
    {
        // User clicked on a tile

        // Is there a piece on this position?
        Piece p = ActivePieces.AsQueryable().Where(x => x.BoardPosition == t.BoardPosition).FirstOrDefault<Piece>();

        // If no piece is selected 
        if (State == BoardState.Ready)
        {
            // if there is a piece at selected position
            if (p != null)
            {
                // check if player is selecting own piece
                if (p.Color == boardSide)
                {
                    // set piece's selected flag
                    p.Selected = true;

                    // set tile's selected tag
                    t.Selected = true;


                    State = BoardState.PieceSelected;
                }
            }
        }

        // If a piece is selected
        else if (State == BoardState.PieceSelected)
        {
            if (SelectedPiece != null)
            {
                SelectedPiece.FuturePosition = t.BoardPosition;

                // check can move
                if (SelectedPiece.CheckCanMove(t.BoardPosition) && !this.IsOwnKingChecked(SelectedPiece.Color))
                {
                    // Piece being taken?
                    Piece eatenPiece = PieceAtPosition(t.BoardPosition);
                    if (eatenPiece != null) { Destroy(eatenPiece.gameObject); }

                    BoardPosition moveStart = SelectedPiece.BoardPosition.Clone();
                    BoardPosition moveEnd = t.BoardPosition;

                    SelectedPiece.MoveToTile(t);

                    FireMovePlayed(SelectedPiece, moveStart, moveEnd);
                }
                else
                {
                    // can not move, return
                    SelectedPiece.MoveToTile(SelectedPiece.CurrentTile);
                }

                State = BoardState.Ready;

                Tile selectedTile = SelectedTile;
                if (selectedTile != null)
                {
                    selectedTile.Selected = false;
                }
            }           
         }
     }

    public void TileRightClickedEventHandler(Tile t)
    {

    }

    #endregion

    void Start()
    {
        State = BoardState.Ready;        
        GameManager = GameObject.FindWithTag("GameController").GetComponent<GameManager>();
        BoardHolder = GameObject.Find("BoardHolder");

        InitBoard();       
    }

    void Update()
    {
        if (boardSide == PieceColor.White)
        {
            Quaternion desiredRotation = Quaternion.Euler(0, 0, 0);
            BoardHolder.transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, smoothTime);
        }
        else
        {
            Quaternion desiredRotation = Quaternion.Euler(0, 180, 0);
            BoardHolder.transform.rotation = Quaternion.Lerp(transform.rotation, desiredRotation, smoothTime);
        }
    }   

    #region Init code

    private void InitBoard()
    {
        boardSide = PieceColor.White;       

        InitTiles();
        InitPieces();
    }

    private void InitTiles()
    {
        GameObject tilesHolder = this.transform.Find("Tiles").gameObject;

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                GameObject tileObj = Instantiate(TilePrefab, tilesHolder.transform);
                Tile tile = tileObj.GetComponent<Tile>();

                BoardPosition TilePosition = new BoardPosition(i + 1, j + 1);

                tile.BoardPosition = TilePosition;
                tile.transform.position += TilePosition.Vector;


                tile.TileClicked += TileClickedEventHandler;
                tile.TileRightClicked += TileRightClickedEventHandler;

            }
        }
    }

    private void InitPieces()
    {
        GameObject piecesHolder = this.transform.Find("Pieces").gameObject;

        // White pieces
        // ************

        // white pawns       

        for (int i = 1; i < 9; i++)
        {   
            CreatePiece(PieceType.Pawn, PieceColor.White, piecesHolder.transform, new BoardPosition(i, 2));
        }

        // white rooks

        CreatePiece(PieceType.Rook, PieceColor.White, piecesHolder.transform, new BoardPosition(1, 1));
        CreatePiece(PieceType.Rook, PieceColor.White, piecesHolder.transform, new BoardPosition(8, 1));

        // white knights

        CreatePiece(PieceType.Knight, PieceColor.White, piecesHolder.transform, new BoardPosition(2, 1));
        CreatePiece(PieceType.Knight, PieceColor.White, piecesHolder.transform, new BoardPosition(7, 1));

        // white bishops

        CreatePiece(PieceType.Bishop, PieceColor.White, piecesHolder.transform, new BoardPosition(3, 1));
        CreatePiece(PieceType.Bishop, PieceColor.White, piecesHolder.transform, new BoardPosition(6, 1));

        // white queen

        CreatePiece(PieceType.Queen, PieceColor.White, piecesHolder.transform, new BoardPosition(4, 1));

        // white king

        CreatePiece(PieceType.King, PieceColor.White, piecesHolder.transform, new BoardPosition(5, 1));

        // Black Pieces
        // ************

        // black pawns       

        for (int i = 1; i < 9; i++)
        {
            CreatePiece(PieceType.Pawn, PieceColor.Black, piecesHolder.transform, new BoardPosition(i, 7));
        }

        // black rooks

        CreatePiece(PieceType.Rook, PieceColor.Black, piecesHolder.transform, new BoardPosition(1, 8));
        CreatePiece(PieceType.Rook, PieceColor.Black, piecesHolder.transform, new BoardPosition(8, 8));

        // black knights

        CreatePiece(PieceType.Knight, PieceColor.Black, piecesHolder.transform, new BoardPosition(2, 8));
        CreatePiece(PieceType.Knight, PieceColor.Black, piecesHolder.transform, new BoardPosition(7, 8));

        // black bishops

        CreatePiece(PieceType.Bishop, PieceColor.Black, piecesHolder.transform, new BoardPosition(3, 8));
        CreatePiece(PieceType.Bishop, PieceColor.Black, piecesHolder.transform, new BoardPosition(6, 8));

        // black queen

        CreatePiece(PieceType.Queen, PieceColor.Black, piecesHolder.transform, new BoardPosition(4, 8));

        // black king

        CreatePiece(PieceType.King, PieceColor.Black, piecesHolder.transform, new BoardPosition(5, 8));

    }

    private GameObject CreatePiece(PieceType type, PieceColor color, Transform parent, BoardPosition boardPosition)
    {
        GameObject piece = null;

        switch (type)
        {
            case PieceType.Pawn:             

                if (color == PieceColor.White) { piece = Instantiate(WhitePawnPrefab, parent); }
                else { piece = Instantiate(BlackPawnPrefab, parent); }

                break;

            case PieceType.Rook:

                if (color == PieceColor.White) { piece = Instantiate(WhiteRookPrefab, parent); }
                else { piece = Instantiate(BlackRookPrefab, parent); }

                break;

            case PieceType.Knight:

                if (color == PieceColor.White) { piece = Instantiate(WhiteKnightPrefab, parent); }
                else { piece = Instantiate(BlackKnightPrefab, parent); }

                break;

            case PieceType.Bishop:

                if (color == PieceColor.White) { piece = Instantiate(WhiteBishopPrefab, parent); }
                else { piece = Instantiate(BlackBishopPrefab, parent); }

                break;

            case PieceType.Queen:

                if (color == PieceColor.White) { piece = Instantiate(WhiteQueenPrefab, parent); }
                else { piece = Instantiate(BlackQueenPrefab, parent); }

                break;

            case PieceType.King:

                if (color == PieceColor.White) { piece = Instantiate(WhiteKingPrefab, parent); }
                else { piece = Instantiate(BlackKingPrefab, parent); }

                break;
        }

        if (piece != null)
        {         
            piece.transform.position += boardPosition.Vector;

            // set up piece script

            Piece pc = piece.GetComponent<Piece>();

            pc.BoardPosition = boardPosition;
            pc.InitialPosition = boardPosition;
          
            // turn black pieces around

            if (pc.Color == PieceColor.Black)
            {
                piece.transform.Rotate(Vector3.up, 180);
            }

            pc.CurrentTile = Tiles.Where(x => x.BoardPosition == pc.BoardPosition).FirstOrDefault<Tile>();

        }

        return piece;
    }

    #endregion

    public void SwitchPlaces()
    {       
        if (boardSide == PieceColor.White)
        {
            boardSide = PieceColor.Black;
            currentRotAngle = blackRotAngle;
        }
        else
        {
            boardSide = PieceColor.White;
            currentRotAngle = whiteRotAngle;
        }
    }

    public bool IsOwnKingChecked(PieceColor kingColor)
    {
        bool isChecked = false;

        List<Piece> enemies = ActivePieces.Where(x => x.Color != kingColor).ToList<Piece>();
        Piece king = ActivePieces.Where(x => x.Color == kingColor && x.Type == PieceType.King).First();

        BoardPosition kingPosition = king.CheckRelevantPosition;
        foreach (Piece e in enemies)
        {
            if (e.CheckCanMove(kingPosition, true))
            {
                isChecked = true;
                break;
            }
        }

        return isChecked;
    }
}
