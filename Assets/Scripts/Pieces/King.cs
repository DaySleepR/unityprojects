﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Pieces
{
    public class King : Piece
    {
        public override bool CheckCanMove(BoardPosition dst, bool kingCheckMove = false)
        {
            bool ret = false;

            BoardPosition src = this.BoardPosition;

            if (dst == src) return false;

            if (
                (Math.Abs(dst.column - src.column) == 0 || Math.Abs(dst.column - src.column) == 1)
                &&
                ((Math.Abs(dst.row - src.row) == 0 || Math.Abs(dst.row - src.row) == 1))
                )
            {
                if (CanTakeAtPosition(dst, kingCheckMove))
                {
                    ret = true;
                }
            }

            return ret;
        }
    }
}
