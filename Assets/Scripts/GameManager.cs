﻿using Assets.Scripts;
using Assets.Scripts.Persistance;
using Assets.Scripts.Pieces;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public interface IGameManager
{
    Player Player1 { get; set; }
    Player Player2 { get; set; }

    Player ActivePlayer { get; set; }
}

public class GameManager : MonoBehaviour,IGameManager
{
    public Player Player1 { get; set; }
    public Player Player2 { get; set; }

    public Player ActivePlayer { get; set; }

    public GameState State { get; set; }

    public IBoard Board { get; set; }

    [Inject]
    public IGameLogger Log { get; private set; }

    GameObject BoardHolder;

    public GameManager()
    {
    }

    public void MovePlayedHandler(Piece piece, BoardPosition origin, BoardPosition destination)
    {
        if (ActivePlayer.Color == PieceColor.Black)
        {
            ActivePlayer = Player1;
        }
        else
        {
            ActivePlayer = Player2;
        }
       
        Board.SwitchPlaces();

        Log.LogMove(origin, destination);
    }

    void Start()
    {
        Board = GameObject.FindWithTag("Board").GetComponent<Board>();
        BoardHolder = GameObject.Find("BoardHolder");

        Board.MovePlayed += MovePlayedHandler;

        State = GameState.Running;

        Player1 = new Player("Player1", PieceColor.White);
        Player2 = new Player("Player2", PieceColor.Black);

        ActivePlayer = Player1;
    }

    void Update()
    {

    }

}
