﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Persistance
{
    public class  Move
    {
        public BoardPosition Src { get; set; }
        public BoardPosition Dst { get; set; }

        public Move()
        {
        }

        public Move(BoardPosition src, BoardPosition dst)
        {
            Src = src;
            Dst = dst;
        }


        public override string ToString()
        {
            if (Src != null && Dst != null)
            {
                return String.Format("{0}-{1}", Src.ToString(), Dst.ToString());
            }
            else
            {
                return "Empty move";
            }
        }



    }
}
